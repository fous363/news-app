import './App.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

// trigger

function App() {
  const [news, setNews] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [searchInputObject, setSearchInputObject] = useState({
    searchBy: "title",
    searchInput: ""
  });

  useEffect(() => {
    axios.get('http://40.121.60.0:8888/api/news')
    .then(function (response) {
      setNews(response.data);
      setIsLoaded(true);
    })
    .catch(function (error) {
      setError(error);
      setIsLoaded(true);
    })
  }, []);

  const handleSearchInputChange = (event) => {
    console.log(searchInputObject);
    setSearchInputObject({
      searchBy: searchInputObject.searchBy,
      searchInput: event.target.value
    });
  };

  const handleSearchByChange = (event) => {
    console.log(searchInputObject);
    setSearchInputObject({
      searchBy:  event.target.value,
      searchInput: searchInputObject.searchInput
    });
  };

  const handleSearchButton = (event) => {
    axios.get('http://40.121.60.0:8888/api/news', { params: {
      [searchInputObject.searchBy]: searchInputObject.searchInput
    }})
    .then(response => {
      setNews(response.data);
    })
    .catch(error => {
      setError(error);
    });
  }

  if (error) {
    return (<div>Error: ${error}</div>);
  } else if (!isLoaded) {
    return (<div>Loading...</div>);
  } else {
    return (
      <div>
        <div>
          <select defaultValue="title" onChange={handleSearchByChange}>
            <option value="title">Название статьи</option>
            <option value="resource">Имя ресурса</option>
          </select>
          <input placeholder="Искать по" onChange={handleSearchInputChange}/>
          <button onClick={handleSearchButton}>Найти</button>
        </div>
        <ul>
        {news.map(newsElement => (
          <li key={newsElement.id}>
            <p className="Title">{newsElement.title}</p>
            <p className="Content">{newsElement.content}</p>
            <a href={newsElement.resourceUrl}>{newsElement.resourceName}</a>
          </li>
        ))}
        </ul>
      </div>
    );
  }
}

export default App;
