FROM node:18.14.0-alpine as build
COPY package.json package-lock.json ./
RUN npm ci --silent
RUN npm install && mkdir /react-ui && mv ./node_modules ./react-ui
WORKDIR /react-ui
COPY . ./
RUN npm run build

FROM nginx:alpine
COPY nginx.conf /etc/nginx.nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /react-ui/build /usr/share/nginx/html
EXPOSE 3000 80
CMD ["nginx", "-g", "daemon off;"]